import csv
from tkinter import filedialog
from icecream import ic

def load_csv():
    tmp_data = []
    with open(filedialog.askopenfilename(), 'r') as file:
        csv_reader = csv.reader(file)

        for row in csv_reader:
            tmp_data.append(row)
    return tmp_data

def print_csv(data):
    for row in data:
        ic(row)
    
