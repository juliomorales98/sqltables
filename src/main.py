from erd_functions import *
from csv_functions import *

if __name__ == "__main__":
    data = load_csv()
    create_uml_diagram(data)
