from graphviz import Digraph
from icecream import ic
from tkinter import simpledialog
import uuid

def create_uml_diagram(data):
    # Create a Digraph object
    dot = Digraph(comment='')
    dot.graph_attr['layout'] = 'osage'
    dot.graph_attr['splines'] = 'ortho'
    dot.graph_attr['nodesep'] = '100'  # Horizontal space between nodes
    
    tabla = ""
    props = ""
    relationships = []  # To store relationships (edges)
    
    for row in data:
        # Handle new table
        if tabla == "":
            tabla = row[0].replace('\ufeff', '')
        elif row[0] != tabla:
            # Add the table to the diagram as a node
            dot.node(tabla, shape='rectangle', label='<<'+tabla.upper()+'>>\n'+props,fontsize='5')
            tabla = row[0]
            props = ""

        parenthesis = False
        foreign_key_table = None  
        foreign_key_column = None  
        
        # Process row to generate fields and identify relationships
        is_pk = False
        for i in range(len(row)):
            if i > 0 and len(row[i]) > 0:
                if i == 3:  # Add parentheses around data type
                    props = props + "("
                    parenthesis = True
                elif i == 4:  # Add a comma (e.g., for composite fields)
                    props = props + ","
                elif i == 6:  # Foreign key table (if present in your data)
                    foreign_key_table = row[i]
                elif i == 7:  # Foreign key column (if present in your data)
                    foreign_key_column = row[i]
                elif i == 5 and row[i] == 'T':
                    is_pk = True
                if i < 5:
                    props = props + row[i] + (" " if i < 2 else "")
        if parenthesis:
            props = props + ")"
            if is_pk:
                props = props + " PK"
            parenthesis = False
        props = props + "\n"

        # If foreign key information is available, add it to the relationships list
        if foreign_key_table and foreign_key_column:
            relationships.append((tabla, foreign_key_table, foreign_key_column))

    # Add the last table
    if len(props) > 0:
        dot.node(tabla, shape='rectangle', label='<<'+tabla.upper()+'>>\n'+props,fontsize='5')

    # Add relationships (edges) to the diagram
    for relation in relationships:
        parent_table, foreign_key_table, foreign_key_column = relation
        dot.edge(parent_table, foreign_key_table, label=f'{foreign_key_column}',fontsize='5',arrowsize='0.2',penwidth='0.2')

    # Render the diagram
    dot.render(simpledialog.askstring("SQLTables", "Name of pdf output (without extension):"), format='pdf', cleanup=True)

