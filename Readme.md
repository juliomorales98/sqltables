# SQLTables

**SQLTables** is a program to generate **ER** diagramas with **graphviz** and **python**.

## Current features

#### Generate ERD diagram with data from a csv with relationships

Currently the program asks for a csv file and generates a erd diagram in a png file with an unique name and the data from the csv. The structure of the csv must be like this:

1. Name of the table

2. Name of the field

3. Type of the field

4. Size of the field( when necesary)

5. Decimal positions of the field (when necesary)

6. Field is Primary Key

7. Table of Foreign Key

8. Foreign key

## Coming features

* Create ER diagram from *sql create*  statement

* Return *sql create* statements from ER and csv file


